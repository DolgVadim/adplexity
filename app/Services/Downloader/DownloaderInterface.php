<?php
/**
 *
 */

namespace App\Services\Downloader;


interface DownloaderInterface
{
    public function download($filePath);
}