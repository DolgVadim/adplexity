<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 11.12.18
 * Time: 10:21
 */

namespace App\Services\Downloader;

class MockDownloader implements DownloaderInterface
{
    public static $mockDownloadLink = 'test.ts';

    public function download($filePath)
    {
        if ($filePath=='success') {
            return self::$mockDownloadLink;
        }

        return false;
    }
}