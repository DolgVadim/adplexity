<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 11.12.18
 * Time: 10:21
 */

namespace App\Services\Downloader;

use Illuminate\Support\Facades\Storage;

class UriDownloader implements DownloaderInterface
{
    public function download($filePath)
    {
        //make file name
        $fileName = pathinfo($filePath, PATHINFO_FILENAME);
        $fileExtension = pathinfo($filePath, PATHINFO_EXTENSION);
        $tmpFile = tempnam(storage_path('tmp'), $fileName . '-');
        if (!empty($fileExtension))
            $tmpFile .= '.' . $fileExtension;

        //download file
        $fp = fopen($tmpFile, 'w+');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        curl_setopt($ch, CURLOPT_URL, $filePath);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        fclose($fp);

        //move downloaded file to public storage
        if ($result) {
            $fileName = pathinfo($tmpFile, PATHINFO_BASENAME);
            Storage::disk('local')->move('tmp/' . $fileName, 'app/public/' . $fileName);
            return Storage::disk('public')->url($fileName);
        }

        return false;
    }
}