<?php

namespace App\Console\Commands;

use App\DownloadJob;
use Illuminate\Console\Command;

class DownloadFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'file:download {uri}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Downloads file by uri';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $downloadJob = new DownloadJob();
        $downloadJob ->uri = $this->argument('uri');
        $downloadJob->save();
        $downloadJob->refresh();
        dispatch(new \App\Jobs\DownloadFile($downloadJob));
    }
}
