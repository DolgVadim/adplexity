<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $downloaderClass = 'App\Services\Downloader\UriDownloader';

        if ($this->app->environment() == 'testing')
            $downloaderClass = 'App\Services\Downloader\MockDownloader';

        $this->app->bind(
            'App\Services\Downloader\DownloaderInterface',
            $downloaderClass
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
