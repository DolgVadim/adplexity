<?php

namespace App\Http\Controllers;

use App\DownloadJob;
use Illuminate\Http\Request;

class DownloadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(DownloadJob::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (empty($fileUri = $request->get('fileURI')))
            return response(['status' => 'error', 'message' => 'pass fileURI parameter']);
        $this->addJob($fileUri);
        return response(['status' => 'ok', 'message' => 'job added']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response(DownloadJob::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function web(Request $request)
    {
        if (!empty($fileUri = $request->get('fileURI')))
        $this->addJob($fileUri);
        return redirect('/');
    }

    protected function addJob($uri)
    {

        $downloadJob = new DownloadJob();
        $downloadJob->uri = $uri;
        $downloadJob->save();
        $downloadJob->refresh();
        dispatch(new \App\Jobs\DownloadFile($downloadJob));
    }
}
