<?php

namespace App\Jobs;

use App\Services\Downloader\DownloaderInterface;
use App\DownloadJob;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class DownloadFile implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $downloadJob ;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(DownloadJob $downloadJob)
    {
        $this->downloadJob = $downloadJob;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(DownloaderInterface $downloader)
    {
        $this->downloadJob ->status = 'downloading';
        $this->downloadJob ->save();

        $result = $downloader->download($this->downloadJob->uri);
        if($result){
            $this->downloadJob ->status = 'complete';
            $this->downloadJob ->download_link = $result;
            $this->downloadJob ->save();
        }
        else{
            $this->downloadJob ->status = 'error';
            $this->downloadJob ->save();
        }
    }
}
