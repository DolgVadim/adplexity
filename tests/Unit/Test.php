<?php

namespace Tests\Unit;

use App\DownloadJob;
use App\Jobs\DownloadFile;
use App\Services\Downloader\MockDownloader;
use Illuminate\Support\Facades\Queue;
use Tests\Unit\Helpers\MyTester;


class Test extends MyTester
{
    /**
     * @test
     */
    public function check_download_process()
    {
        Queue::fake();

        $downloadJob = new DownloadJob();
        $downloadJob->uri = 'success';
        $downloadJob->save();

        //check creation
        $jobFromDb = DownloadJob::first();
        $this->assertArrayHasKeys($jobFromDb->toArray(), ['uri', 'status', 'download_link']);
        $this->assertEquals($jobFromDb->uri, 'success');
        $this->assertEquals($jobFromDb->status, 'pending');
        $this->assertEquals($jobFromDb->download_link, null);

        //check dispatch
        DownloadFile::dispatch($jobFromDb);
        Queue::assertPushed(DownloadFile::class, function ($job) use ($downloadJob) {
            return $job->downloadJob->id === $downloadJob->id;
        });

        //check statuses
        $downloadJob2 = new DownloadJob();
        $downloadJob2->uri = 'fail';

        DownloadFile::dispatchNow($downloadJob);
        $this->assertEquals($downloadJob->uri, 'success');
        $this->assertEquals($downloadJob->status, 'complete');
        $this->assertEquals($downloadJob->download_link, MockDownloader::$mockDownloadLink);

        DownloadFile::dispatchNow($downloadJob2);
        $this->assertEquals($downloadJob2->uri, 'fail');
        $this->assertEquals($downloadJob2->status, 'error');
        $this->assertEquals($downloadJob2->download_link, null);
    }

}

