## Test task

.env.example contains my original .env for testing.

symlink public/storage -> storage/app/public need to be created.

## Usage

command line:

php artisan file:show - job list

php artisan file:download {uri} - start download job

api:

GET http://{host}/api/file/ - job list

POST http://{host}/api/file/ - start download job (fileURI={uri})

web:

http://{host} - job list

submit form with uri - start download job
