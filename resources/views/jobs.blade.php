<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Jobs status</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

    <!-- Styles -->
</head>
<body>
<nav class="navbar navbar-light bg-dark mb-5">
    <span class="navbar-brand text-light mb-0 h1">Adpexity: Job list</span>
</nav>
<div class="container">
    <form class="mb-5" method="post">
        @csrf
        <div class="form-group">
            <input type="text" class="form-control" id="file" name="fileURI" placeholder="Paste file URI">
        </div>
        <button type="submit" class="btn btn-primary">Download file</button>
    </form>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">URI</th>
            <th scope="col">Status</th>
            <th scope="col">Download link</th>
        </tr>
        </thead>
        <tbody>
        @php
            $classMap =[
            'pending'=>'info',
            'error'=>'danger',
            'complete'=>'success',
            'downloading'=>'warning'
            ];
        @endphp
        @foreach($jobs as $job)
            <tr>
                <th scope="row">{{$job->id}}</th>
                <td><a href="{{$job->uri}}" target="_blank">{{$job->uri}}</a></td>
                <td><span class="alert alert-<?= $classMap[$job->status]; ?>">{{$job->status}}</span></td>
                <td><a href="{{$job->download_link}}" target="_blank">{{$job->download_link}}</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</body>
</html>
